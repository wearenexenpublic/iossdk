# README #

WeAreNexen SDK v1.12.64 for iOS

## What is this repository for ##

This repository contains the library you need to interact with WeAreNexen Proximity Zones

## Requirements ##

The minimum iOS target is 8.0, and you need an iOS device with Bluetooth LE support for it to function correctly.

### Disclaimer! ###
It is important to have a good understanding of the way iOS CoreLocation handles beacons in foreground and background, and the practical implications this has in order to understand the behaviour of the SDK. There are quite a few special cases due to the way iOS works, so unfortunately this SDK cannot be considered as a simple black box for someone how doesn't know or doesn't want to know anything about the usage of iBeacons on iOS. Lack of knowledge will lead to false assumptions and a lot of time spent debugging issues that are normal behaviour for the platform.

## Setup ##

Download the sources and include the project as a subproject in your iOS app. Add the `NXBeaconSDK` framework as an Embedded Binary in your target settings (in the general tab).

Do not forget to enable the following capabilities in your target
- Background modes for location updates, Uses Bluetooth LE accessoires and Background fetch

### Xcode Project Settings ###

In order for everything to work correctly your app should have the following capabilities enabled in Target/Capabilities/Background Modes

* Uses Bluetooth LE accessoires
* Background fetch
  
Since we require background location updates your app should also have the following entries in its plist file: `NSLocationAlwaysUsageDescription`, `NSLocationWhenInUseUsageDescription` and `NSLocationAlwaysAndWhenInUseUsageDescription`

Its value should be a string explaining why you request location services (here you can explain to the user that the location services are used to monitor iBeacons for instance).

## Implementation ##

### Setup ###

Before using the SDK, the app must have acquired permission to always use location services, even when the app is in background. In iOS terms this means:

`[CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways`

for any other value than `kCLAuthorizationStatusAuthorizedAlways` the SDK will simply not work, and it's up to the developer to make sure the right authorization status is given to the app before using the SDK.


If you have acquired location permissions, to use the SDK you need to instantiate the `NXProximityManager` class as follows:

```
[[NXProximityManager alloc]initWithUsername:username password:password locale:locale];
```

`username` and `password` are values you will receive from Nexen. 

`locale` is an NSLocale instance that represents the language you want to use to receive the proximity zone's contents. Only the language part of the locale will be taken into consideration, so for now it doesn't make a difference if the locale is `fr-be` or `fr-fr`.

Because this SDK performs tasks in the background, and all background app interaction goes through your `UIApplication` delegate, you need to add the following code to your delegate

```
- (void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult result))completionHandler
{
    [proximityManager performFetchWithCompletionHandler:completionHandler];
}

- (void)application:(UIApplication *)application handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)())completionHandler
{
    [proximityManager handleEventsForBackgroundURLSession:identifier completionHandler:completionHandler];
}
```

where `proximityManager` is your `NXProximityManager` instance

### Running ###

To start monitoring call `start` on the `NXProximityManager` instance. This will start the periodic synchronisation with the backend, and launch the beacon monitoring.

This method takes a completion block as only parameter. If the completion block is executed and the `NSError * error` parameter is `nil`, everything is running correctly. Otherwise something went wrong during startup.

If you want to know if the SDK was started you can do so by checking the value of the `started` property.

Some events in the SDK are exposed to the main app using standard `NSNotification` objects.

The notifications one can subscribe to are:

* `NXProximityManagerLocationsUpdatedNotification`
* `NXProximityManagerProximityZonesForLocationUpdatedNotification`
* `NXProximityManagerRefreshErrorNotification`
* `NXProximityManagerTriggerContentNotification`

All notifications are dispatched on the main thread.


#### NXProximityManagerLocationsUpdatedNotification

This notification is dispatched every time the SDK updates the location list. The updated locations can be found in the `locations` property of the `NXProximityManager` instance.

#### NXProximityManagerProximityZonesForLocationUpdatedNotification

After loading the proximity zones for a location this notification is dispatched. The actual location can be found by calling the class method `[NXProximityManager locationFromNotification:]` and passing the `NSNotification` as parameter.

#### NXProximityManagerRefreshErrorNotification

Every time something goes wrong when trying to update data coming from the server this notification is dispatched. The `NSError` object can be found by calling `[NXProximityManager errorFromNotification:]` and passing the `NSNotification` as parameter.

Receiving this notification does not mean that the library is no longer functioning correctly due to an error. It just means that something went wrong, but it is perfectly possible that a user will never notice this. An example might be that resyncing the proximity zones of a certain location has failed. The user of your app might not notice this as the app will continue functioning with the current list of proximity zones, and will try refreshing that list later on.

#### NXProximityManagerTriggerContentNotification

When a proximity zone is detected and the constraints are met for its contents to trigger, the app will receive a `NXProximityManagerTriggerContentNotification` 

You can get the proximity zone associated to the notification by using

`+(NSObject<NXProximityZone> *)proximityZoneFromNotification:(NSNotification *)notification`

on `NXProximityManager`

#### Showing contents

In order to correctly manage the beacon contents on client and server side it is important to use the following calls on the current proximity zone as soon as contents are shown:

On `NXProximityZoneContent`  (the `content` property of a proximity zone) call `-(void)trigger;` to make sure the beacon state is updated to the triggered state. This is important so repeat delay and other properties work as expected.

On `NXProximityManager` call `-(void)incrementLoyaltyForProximityZone:(NSObject<NXProximityZone> *)zone;` in order to update the server-side stats

#### Handling content example

This is some example code that shows how to handle proximity zone content, taking in account the fact that the app can be in the background when the notification is sent

```
-(void)contentTriggered:(NSNotification *)notification
{
    NSObject<NXProximityZone> * beacon = [NXProximityManager proximityZoneFromNotification:notification];
    
    if([UIApplication sharedApplication].applicationState == UIApplicationStateBackground)
    {
        UILocalNotification * notif = [[UILocalNotification alloc] init];
        notif.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
        notif.alertBody = beacon.content.message;
        notif.soundName = UILocalNotificationDefaultSoundName;
        notif.userInfo = @{@"beaconIdentifier":beacon.identifier};
        NSLog(@"Scheduling Push notification");
        
        [[UIApplication sharedApplication] scheduleLocalNotification:notif];
    }
    else
    {
        [self showProximityZoneContent:beacon];
        
    }
}

-(void)showProximityZoneContent:(NSObject<NXProximityZone> *)beacon
{
    if (beacon)
    {
        [beacon.content trigger];
        [self.beaconManager incrementLoyaltyForProximityZone:beacon];

        /* HANDLE ACTUAL CONTENT SHOWING HERE */        

    }
}

-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    NSString * identifier = notification.userInfo[@"beaconIdentifier"];
    
    NSObject<NXProximityZone> * beacon = [self.beaconManager proximityZoneForIdentifier:identifier];

    [self showProximityZoneContent:beacon];
}

```

### Configuration

#### NXSettingsManager
`NXProximityManager` exposes a property `settingsManager` of type `NXSettingsManager`. This property can be used to select which proximity zone content categories should be monitored by the SDK. This settings manager has an `NSArray` of `NXProximityZoneContentCategory` instances. 

Enabling or disabling a category can be done by passing one of those instances to `-enableContentCategory:enabled:` on the `NXSettingsManager` instance.

Checking if a content category is enabled can be done by calling `contentCategoryEnabled:`

Settings are automatically persisted by the SDK.

If you want to explicitly refresh the list of content categories, call `refresh:` on  the `settingsManager`.

### Tags

Tags are located in NXSettingsManager

- (void)setTag:(NSString *)tagName withValue:(NSString *)value 
Add new / or replace existing tag value, stored by the tagName

- (void)deleteTag:(NSString *)tagName
Remove tag by it's name (if exists)

- (void)clearTags
Remove all tags 

All tags are saved in caches directory

### Debugging

Calling `[NXProximityManager enableLogging:]` will enable or disable debug output to the console. By default nothing is logged.

Calling `reset` on the `NXProximityManager` instance will remove all cached files and all monitored regions from the device. Calling `reset` will make the instance you call this on useless for any further usage. Should only be used when developing.

## Geofences ##

Geofences logic will work in the same case as Beacons 
App download prefetched geofences and trigger spectific notifications


### NXProximityManager

```
- (NSArray <NXProximityZone> *)enteredZones;
```
list of beacons and geofences, that currently in range

```
- (void)showNotificationForProximityZone:(NSObject<NXProximityZone> *)zone; 
```
run local notification for provided zone content
zone id, and content type could be found in userInfo dictionary